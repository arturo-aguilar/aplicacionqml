import QtQuick 2.0
import QtQuick.Controls 2.5
import QtWebSockets 1.15
import QtMultimedia 5.15
//import Felgo 3.0

ApplicationWindow {
    visible: true
    visibility: "Maximized"
    //width: imgFondo.width;height: imgFondo.height
    id:root
    property string nomUser: "Arturo"
    property string fotoPerfil: "foto-perfil.jpeg"
    property string text: ""
    property string urlImg: ""

    Image {
        //width: 360;height: 700
        width: root.width;height: root.height
        id: imgFondo
        source: "assets/rect956.png"
    }
    Text {
        id: tittle
        text: qsTr("Rondas")
        font.pixelSize: 30
        color: "white"
        anchors.top: estado.bottom
        //anchors.left: txtmuro.left
        font.family: webFont.name
        font.weight: Font.Medium
        x:10
    }
    Image {
        id: linea
        width: parent.width
        source: "assets/path1062.png"
        anchors.top: tittle.bottom
    }

    Rectangle{
        id:estado
        anchors.topMargin: 25
        y:3
        anchors.right: parent.right
        anchors.rightMargin: 3
        width: 30;height: width
        color: "#85D55D"
        radius: 30
        border.color: "white"
        border.width: 6
        MouseArea{
            anchors.fill: parent
            onClicked: {
                //socket.active = !socket.active
                tomarFoto.visible=true
            }
        }
    }

    //esta es el area de texto
    Rectangle{
        id:txttexto
        visible: true
        width:imgFondo.width-send.width-50;height: 85
        //color: "green"
        //anchors.left: txtmuro.left
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        anchors.leftMargin: 25
        //focus: true
        x:tittle.x
        clip: false

        TextArea{
            id:txtentarada
            //width: txttexto.width;height: txttexto.height
            //autoScroll: true
            anchors.fill: parent
            focus: false
            font.pixelSize: 16
            wrapMode: TextEdit.Wrap
            selectByMouse: true
            anchors.margins: 10
            clip: false
            font.family: webFont1.name
            font.weight: Font.Medium
            persistentSelection: true
            //placeholderText: "Mensaje"
            onTextChanged: {
                deselect()
                console.log(txtentarada.text)
                var pos = txtentarada.positionAt(1, txttexto.height-15);
                if(txtentarada.length >= pos-1)
                {
                    txtentarada.remove(pos, txtentarada.length);
                }
            }
        }

        Image {
            id: toptexto
            width: parent.width
            source: "assets/image8862.png"
            y:-12

        }
        Image {
            id: bottomtexto
            width: parent.width
            source: "assets/image8862.png"
            y:15
            rotation: 180
        }


    }
    //objeto para indicar q67c111ue no hay conexion
    Rectangle{
        id:outline
        visible: false
        color: "#BABDB6"
        //color: "green"
        width:root.width-30;height: txttexto.height/2
        //anchors.left: txtmuro.left
        //anchors.fill: parent
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 10
        //anchors.rightMargin: 25

        x:tittle.x
        clip: true
        Image {
            id: toptexto1
            width: parent.width
            //anchors.verticalCenter: parent.verticalCenter
            source: "assets/image8862.png"
            y:-11.5

        }
        Image {
            id: bottomtexto1
            width: parent.width
            //anchors.verticalCenter: parent.verticalCenter
            source: "assets/image8862.png"
            y:-28
            rotation: 180
        }


    }

    /*Rectangle{

        id:txtmuro
        Image {
            id: fondo
            anchors.fill: parent
            //source: "assets/rect956.png"
        }
        color: "gray"
        width: 340;height: 400
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: linea.bottom
        anchors.topMargin: 30*/
    ListView {
        id:lista
        anchors.fill: parent
        anchors.topMargin: 72
        anchors.bottomMargin: 100
        anchors{
            rightMargin: 15
            leftMargin: 15
        }

        clip: true

        delegate: numberDelegate
        model: publicaciones
        spacing: 15
        focus: true

        onCountChanged: {
            var newIndex = count - 1 // ultimo elemento
            currentIndex = newIndex
        }

    }
    ListModel{
        id:publicaciones
        /*ListElement{id_usu:1; name:"Juan Perez"; facts:"Las propiedades adjuntas desde la vista más utilizadas son ListView.isCurrentItemy ListView.view. El primero es un valor booleano que indica si el elemento es el elemento actual, mientras que el último es una referencia de solo lectura a la vista real. A través del acceso a la vista, es posible crear delegados generales reutilizables que se adapten al tamaño y naturaleza de la vista en la que están contenidos.";imageSource:"assets/foto-portada.jpg";image:"assets/foto-portada.jpg"}
            ListElement{id_usu:1; name:"Juan Perez"; facts:"Las propiedades adjuntas desde la vista más utilizadas son ListView.isCurrentItemy ListView.view. El primero es un valor booleano que indica si el elemento es el elemento actual, mientras que el último es una referencia de solo lectura a la vista real. A través del acceso a la vista, es posible crear delegados generales reutilizables que se adapten al tamaño y naturaleza de la vista en la que están contenidos.";imageSource:"assets/foto-portada.jpg";image:"assets/foto-portada.jpg"}
            ListElement{id_usu:1; name:"Juan Perez"; facts:"Las propiedades adjuntas desde la vista más utilizadas son ListView.isCurrentItemy ListView.view. El primero es un valor booleano que indica si el elemento es el elemento actual, mientras que el último es una referencia de solo lectura a la vista real. A través del acceso a la vista, es posible crear delegados generales reutilizables que se adapten al tamaño y naturaleza de la vista en la que están contenidos.";imageSource:"assets/foto-portada.jpg";image:"assets/foto-portada.jpg"}
            ListElement{id_usu:1; name:"Juan Perez"; facts:"Las propiedades adjuntas desde la vista más utilizadas son ListView.isCurrentItemy ListView.view. El primero es un valor booleano que indica si el elemento es el elemento actual, mientras que el último es una referencia de solo lectura a la vista real. A través del acceso a la vista, es posible crear delegados generales reutilizables que se adapten al tamaño y naturaleza de la vista en la que están contenidos.";imageSource:"assets/foto-portada.jpg";image:"assets/foto-portada.jpg"}
            */
    }


    Component {
        id: numberDelegate

        Rectangle {
            id:txtelmuro
            width: lista.width-60;//height: nombreUsuario.height+fotoperfil.height+prueba.height+imgGrande.height+45//45
            height: imgGrande.source!=""?nombreUsuario.height+fotoperfil.height+prueba.height+imgGrande.height+45:nombreUsuario.height+fotoperfil.height+prueba.height+45
            //color: nombreUsuario.text==root.nomUser?"white":"#B9B7B7"
            color: "white"
            x:nombreUsuario.text==root.nomUser?53:0
            //radius: 8
            Text {
                id: nombreUsuario

                font.family: webFont1.name
                font.weight: Font.Medium
                font.pixelSize: 15
                text: name
                x:15;y:15
            }
            Image {
                id: fotoperfil
                source: image
                width: 55;height: 55
                anchors.right: parent.right
                anchors.top: nombreUsuario.top
                anchors.rightMargin: 15
                fillMode: Image.PreserveAspectFit
            }
            TextEdit{
                enabled: false
                id: prueba
                color: "gray"
                text: facts
                font.pixelSize: 15
                anchors.left: nombreUsuario.left
                wrapMode: TextEdit.Wrap
                width: txtelmuro.width-50
                anchors.top: fotoperfil.bottom
                anchors.topMargin: 15
                font.family: webFont1.name;font.weight: Font.Normal


            }
            //contenedor para imagen grande del muro
            Image {
                id: imgGrande
                anchors.top: prueba.bottom
                source: imageSource
                //source: "assets/circulo2.png"
                width: txtelmuro.width;height: 50
                fillMode: Image.PreserveAspectFit
            }

            /*Rectangle{
                        id:contenedor
                        width: txtelmuro.width-30; height: 190
                        color: "green"
                        anchors.top: prueba.bottom
                        anchors.left: prueba.left
                        //anchors.margins: 15
                        anchors.bottomMargin: 15
                        anchors.topMargin: 15
                        visible: false
                        Image {
                            id: imgpost
                            //height: 150; width: auto
                            anchors.fill: parent
                            //anchors.bottom:txtelmuro.bottom
                            //anchors.centerIn: parent
                            source: "assets/flecha.png"
                            //anchors.margins: 25
                        }
                    }*/
            //Animacion de insercion
            ListView.onAdd: SequentialAnimation {
                NumberAnimation { target: txtelmuro; property: "scale"; from: 0; to: 1; duration: 500; easing.type: Easing.OutExpo }
            }
        }
    }
    //delegato para mensajes del servidor
    //}
    Image {
        id: sombra
        visible: false
        source: "assets/image8862.png"
        width: lista.width
        anchors.top: lista.top
        anchors.left: lista.left
        anchors.topMargin: -11.5
        //anchors.horizontalCenter:  lista
    }
    Image {
        id: sombrabottom
        visible: false
        source: "assets/image8862.png"
        width: lista.width
        anchors.top: lista.bottom
        anchors.left: lista.left
        anchors.topMargin: -70
        rotation: 180
        //anchors.verticalCenter: lista
    }
    Image {
        id: send
        visible: true
        width: 40;height: width
        source: "assets/flecha.png"
        anchors.left: txttexto.right
        anchors.verticalCenter: txttexto.verticalCenter
        anchors.leftMargin: 10
        enabled: txtentarada.length>0
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(txtentarada.text!=""){
                    sombra.visible=true
                    sombrabottom.visible=true
                    socket.sendTextMessage(JSON.stringify({"name":root.nomUser,"facts":txtentarada.text,"image":"assets/"+root.fotoPerfil,"imageSource":root.urlImg}))
                    publicaciones.append({"name":root.nomUser,"facts":txtentarada.text,"image":"assets/"+root.fotoPerfil,"imageSource":root.urlImg})
                    txtentarada.text=""
                    root.text=""
                    root.urlImg=""
                }

            }
        }
    }

    //conexion con websocket


    WebSocket {
        id: socket
        url: "ws://192.168.99.113:3011"
        //url: "ws://localhost:3011"
        active: true
        onTextMessageReceived: {
            if(!message==""){
                var mes=JSON.parse(message);
                console.log(mes.name);
                if(mes.name!==root.nomUser){
                    sombra.visible=true
                    sombrabottom.visible=true
                    publicaciones.append({"name":mes.name,"facts":mes.facts,"image":mes.image,"imageSource":mes.imageSource})
                }
            }
        }

        onStatusChanged: {

            if (socket.status == WebSocket.Error) {
                //console.log("Error: " + socket.errorString)
                estado.color="red"
                txttexto.visible=false
                send.visible=false
                outline.visible=true
                tim.start()
            } else if (socket.status == WebSocket.Open) {
                //console.log("conexion"+ socket.status)
                estado.color="#85D55D"
                txttexto.visible=true
                send.visible=true
                outline.visible=false
                socket.sendTextMessage(txtentarada.text)
                tim.stop()

            } else if (socket.status == WebSocket.Closed) {
                estado.color="red"
                txttexto.visible=false
                send.visible=false
                outline.visible=true
                tim.start()
            }
        }
    }

    FontLoader { id: webFont; source: "assets/Rubik-Medium.ttf"}
    FontLoader {id:webFont1; source:"assets/Roboto-Medium.ttf" }

    Timer {
        id: tim;
        interval: 5000;
        repeat: true
        triggeredOnStart: true;
        running: false
        onTriggered: {
            if(socket.status!=1){
                socket.active = !socket.active
            }
        }
    }

    //Para mandar mensajes con fotos
    Rectangle{
        id:tomarFoto
        width: root.width;height: root.height
        color: "black"
        visible: false
        Camera {
            id: camera

            imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

            exposure {
                exposureCompensation: -1.0
                exposureMode: Camera.ExposurePortrait
            }

            flash.mode: Camera.FlashRedEyeReduction

            imageCapture {
                onImageCaptured: {
                    root.urlImg=preview
                    //photoPreview.source = root.urlImg  // Show the preview in an Image
                }
            }
        }
        VideoOutput {
            id:videocamere
            source: camera
            anchors.fill: parent
            orientation: -90
            visible: true
            focus : visible // to receive focus and capture key events when visible
        }
        Button {
            id: shotButton
            width: root.width
            anchors.bottom: parent.bottom
            text: "Capturar"
            onClicked: {
                camera.imageCapture.capture();
            }
        }
        Button {
            id: closeCamera
            width: root.width
            anchors.top: parent.top
            text: "Salir"
            onClicked: {
                tomarFoto.visible=false
            }
        }
    }
}
